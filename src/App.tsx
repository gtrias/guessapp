import React from 'react'
import { Provider } from 'react-redux'

import store from './store/configureStore'
import './App.css'
import GameStage from './components/GameStage'

const App = () => {
  return (
    <Provider store={store}>
      <div className="App flex overflow-hidden">

        <GameStage
          playerName='Genar'
        />

        <div
          id='sense-debugger'
          style={{
            display: 'none',
            position: 'fixed',
            right: 0,
            bottom: 0,
            color: 'white',
            background: 'black',
            padding: '20px',
            fontSize: '2em'
          }}
        />
      </div>
    </Provider>
  )
}

export default App
