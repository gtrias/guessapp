import { put, takeLatest, select } from 'redux-saga/effects'
import slices from '../slices'

export const getTilt = (state: any) => {
  return state?.tilt
}

function* callNext(action: any) {
  const prevTilt = yield select(getTilt)

  if (prevTilt.action !== action.payload.action) {
    yield put(slices.words.actions.nextWord())
  }
}

export function* words() {
  yield takeLatest(slices.tilt.actions.setTilt.toString(), callNext)
}

export default words
