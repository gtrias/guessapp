import { call } from 'redux-saga/effects'
import slices from '../slices'
import Tilt from '../gestures/Tilt'

const handleTilt = (dispatch: any) => {
  Tilt((data) => {
    console.log('tilt handled')
    dispatch(slices.tilt.actions.setTilt(data))
  })
}

function* listenRotation(dispatch: any) {
  yield call(handleTilt, dispatch)
}

export function* tiltSagas(store: any) {
  yield call(listenRotation, store.dispatch)
}

export default tiltSagas
