import React from 'react'
import { connect } from 'react-redux'
import Fade from 'react-reveal/Fade'

type Props = {
  action: 'DISCARD' | 'ACCEPT',
  word: string,
  playerName: string,
  words: Array<string>,
  wordIndex: number
}

const GameStage = (props: Props) => {

  const getBgColor = () => {
    if (props.action === 'DISCARD') {
      return 'bg-red-400'
    }
    if (props.action === 'ACCEPT') {
      return 'bg-green-400'
    }

    return 'bg-blue-800'
  }

  const styles = {
    display: 'flex',
    width: '100%',
    height: '100vh'
  }

  return (
    <div className={`flex-col justify-center items-center ${getBgColor()}`} style={styles}>
      <Fade top>
        <h1 className='text-2xl md:text-4xl py-6 text-blue-100'>
          Movies
        </h1>

        <div className='card rounded-lg shadow-xl bg-blue-300 px-20 py-12 mx-12'>
          <div className='text-center text-4xl md:text-6xl'>
            { props.word }
          </div>
        </div>
      </Fade>

      <div className='flex justify-around m-12 w-full'>
        <button
          className='btn btn-orange'
        >
          No
        </button>
        <button
          className='btn btn-blue'
        >
          Yes
        </button>
      </div>

      <div className='flex justify-around m-12 w-full'>
        <Fade left>
          <div className='text-2xl p-4 text-blue-100'>
            Player: { props.playerName }
          </div>
        </Fade>
        <Fade right>
          <div className='text-2xl p-4 text-blue-100'>
            { props.wordIndex } / { props.words.length }
          </div>
        </Fade>
      </div>
    </div>
  )
}

export default connect((state: any) => ({
  action: state.tilt.action,
  word: state.words.currentWord,
  words: state.words.words,
  wordIndex: state.words.index
}))(GameStage)
