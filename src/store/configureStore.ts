import { all } from 'redux-saga/effects'
import {
  createStore,
  combineReducers,
  applyMiddleware,
  compose
} from 'redux'
import createSagaMiddleware from 'redux-saga'

import tiltSagas from '../sagas/tilt'
import wordsSaga from '../sagas/words'
import slices from '../slices'

const reducers = combineReducers({
  tilt: slices.tilt.reducer,
  words: slices.words.reducer
})

function* sagas(): any {
  yield all([
    tiltSagas(store),
    wordsSaga()
  ])
}

// configure middleware
const sagaMiddleware = createSagaMiddleware()

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

// create store
export const store = createStore(reducers,
  composeEnhancers(
    applyMiddleware(sagaMiddleware)
  )
)

const words = [
  'Jurassik Park',
  'Quentin Tarantino'
]

store.dispatch(slices.words.actions.loadWords(words))

// run saga
sagaMiddleware.run(sagas as any, store)

export default store
