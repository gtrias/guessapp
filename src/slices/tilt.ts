import { createSlice } from '@reduxjs/toolkit'

const tiltSlice = createSlice({
  name: 'tilt',
  initialState: { action: '', magnitude: 0 },
  reducers: {
    setTilt: (state: any, action: any) => {
      return {
        ...state,
        action: action.payload.action,
        magnitude: action.payload.magnitude
      }
    },
    resetTilt: (state: any, action: any) => {
      return {
        ...state,
        action: 'WAITING',
        magnitude: action.payload.magnitude
      }
    },
  }
})

export default tiltSlice
