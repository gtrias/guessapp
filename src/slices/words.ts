import { createSlice } from '@reduxjs/toolkit'

const words = createSlice({
  name: 'words',
  initialState: { words: [], currentWord: '', index: 0 },
  reducers: {
    loadWords: (state: any, action: any) => {
      return {
        ...state,
        currentWord: action.payload[state.index],
        words: action.payload
      }
    },
    nextWord: (state: any) => {
      return {
        ...state,
        index: state.index + 1,
        currentWord: state.words[state.index + 1]
      }
    },
  }
})

export default words
