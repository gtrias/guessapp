import tilt from './tilt'
import words from './words'

export default {
  tilt,
  words
}
