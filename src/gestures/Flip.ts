import common from './common'

const Flip = (cb: (data: any) => void) => {
  common.checkCompatibilty()

  let gammas: Array<any> = []

  console.log('device orientation event found')

  const options = {
    gestureDuration: 250
  }
  let intervalExpired = false

  setInterval(() => { intervalExpired = true }, options.gestureDuration)

  window.addEventListener('deviceorientation', (eventData) => {
    let final_gamma = 0
    let found = false

    if (intervalExpired) {
      gammas[gammas.length] = eventData.gamma;
      for (let i = 1; i < 5; i++) {
        if (Math.abs(gammas[gammas.length-1] - gammas[gammas.length-1-i]) > 160) {
          found = true
          final_gamma = gammas[gammas.length - 1]
          gammas = []
          break
        }
      }

      intervalExpired = false
    }

    if (found) {
      cb({
        magnitude: Math.round(final_gamma)
      })
    }
  })
}

export default Flip
