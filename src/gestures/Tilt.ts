import common from './common'

const writeDebugInfo = (data: any) => {
  const debugDiv = document?.getElementById('sense-debugger')
  if (debugDiv) {
    debugDiv.innerText = JSON.stringify(data, undefined, 2)
  }
}

const Tilt = (cb: (data: any) => void) => {
  if (common.checkCompatibilty()) {
    window.addEventListener('deviceorientation', (eventData: any) => {
      // const delta = lastSample - eventData.gamma
      const delta = eventData.gamma

      if(delta > -70 && delta < 0) {
        const data = {
          action: "ACCEPT",
          magnitude: Math.round(delta)
        }

        writeDebugInfo(data)
        cb(data)
      }

      if(delta < -70 && delta > 0) {
        const data = {
          action: "WAITING",
          magnitude: Math.round(delta)
        }

        writeDebugInfo(data)
        cb(data)
      }

      if(delta < 60 && delta > 0) {
        const data = {
          action: "DISCARD",
          magnitude: Math.round(delta)
        }

        writeDebugInfo(data)
        cb(data)
      }
    })
  }
}

export default Tilt
