const checkCompatibilty = (): Boolean => {
  if (!window.DeviceOrientationEvent) {
    alert('browser not supported')

    return false
  }

  return true
}

export default {
  checkCompatibilty
}
